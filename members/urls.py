from django.conf.urls import patterns, url


urlpatterns = patterns('',
                       url(r'^se-deconnecter/$', 'django.contrib.auth.views.logout', {'next_page': "/"}, name="logout"),
                    )
