from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Membre(models.Model):
    utilisateur = models.OneToOneField(User)
    amis = models.ManyToManyField("self", related_name="amis_set")
    demandes = models.ManyToManyField("self", blank=True, related_name="demandes_set", symmetrical=False)

    @classmethod
    def create(cls, utilisateur):
        membre = cls(utilisateur=utilisateur)
        return membre

    def __unicode__(self):
        return self.utilisateur.username