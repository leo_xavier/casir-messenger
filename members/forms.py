from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm as DjangoAuthenticationForm

class AuthenticationForm(DjangoAuthenticationForm):
    username = forms.CharField(label="Nom d'utilisateur", max_length=254, widget=forms.TextInput(attrs={"class": "form-control", 'placeholder':'Nom d\'utilisateur'}))
    password = forms.CharField(label="Mot de passe", widget=forms.PasswordInput(attrs={"class": "form-control", 'placeholder':'Mot de passe'}))

class RegistrationForm(forms.Form):
    username = forms.CharField(label="Nom d'utilisateur", max_length=254, widget=forms.TextInput(attrs={"class": "form-control", 'placeholder':'Nom d\'utilisateur'}))
    password = forms.CharField(label="Mot de passe", widget=forms.PasswordInput(attrs={"class": "form-control", 'placeholder':'Mot de passe'}))
    email = forms.CharField(label="Email", max_length=254, widget=forms.EmailInput(attrs={"class": "form-control", 'placeholder':'Email'}))
    
    
class AddFriendsForm(forms.Form):
    users_add = forms.ModelMultipleChoiceField(queryset=User.objects.none(), widget=forms.CheckboxSelectMultiple())
    
    def __init__(self, users):
        super(AddFriendsForm,self).__init__()
        self.fields["users_add"].queryset=users
        
class AcceptFriendsForm(forms.Form):
    users_accept = forms.ModelMultipleChoiceField(queryset=User.objects.none(), widget=forms.CheckboxSelectMultiple())
    
    def __init__(self, users):
        super(AcceptFriendsForm,self).__init__()
        self.fields["users_accept"].queryset=users        
        