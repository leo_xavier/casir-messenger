# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='membre',
            name='amis',
            field=models.ManyToManyField(related_name='amis_set', to='members.Membre'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='membre',
            name='demandes',
            field=models.ManyToManyField(related_name='demandes_set', to='members.Membre', blank=True),
            preserve_default=True,
        ),
    ]
