# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0002_auto_20150402_1126'),
    ]

    operations = [
        migrations.AlterField(
            model_name='membre',
            name='amis',
            field=models.ManyToManyField(related_name='amis_rel_+', to='members.Membre'),
            preserve_default=True,
        ),
    ]
