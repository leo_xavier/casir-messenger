from django.shortcuts import render
from django.contrib.auth import login as auth_login
from django.contrib.auth import authenticate
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from itertools import chain

from members.forms import AuthenticationForm
from members.forms import RegistrationForm
from members.forms import AddFriendsForm, AcceptFriendsForm
from members.models import Membre
from messagerie.models import Message
import json

def chargerMessage(request):
    
    idMsg = request.POST.get("id")
    msg = Message.objects.get(id=idMsg)
    msg.etat = True
    msg.save()
    data = json.dumps({'msg':msg.message, 'duree':msg.duree})
    ntm = {}
    ntm['status'] = 200
    ntm['content_type'] = 'application/json'
    return HttpResponse(data, **ntm)

def conversation(request):
    user = request.user
    membre = Membre.objects.get(utilisateur=user)
    
    if request.method == "GET":
        # Conversation avec un ami
        if "friend" in request.GET :
            idAmi = request.GET.get("friend")
            ami = Membre.objects.get(id=idAmi)
            amisListe = membre.amis.all()
            messages = Message.objects.filter(expediteur=ami).filter(destinataire=membre).all()
    context = {
        'messages':messages,
        'amis':amisListe
    }
    return render(request, "dashboard/conversation.html", context)

def index(request):
    user = request.user
    membre = Membre.objects.get(utilisateur=user)
    
    
    messages = Message.objects.filter(destinataire=membre).all()          
    
    if request.method == "POST":
        
        # Ajouter des gonz
        if "users_add" in request.POST :
            lesUsers = request.POST.getlist("users_add")
            for unIdUser in lesUsers :
                leMembre = Membre.objects.get(utilisateur=unIdUser)
                leMembre.demandes.add(membre)
                leMembre.save()
            
        # Accepter des gonz    
        if "users_accept" in request.POST :
            
            lesUsers = request.POST.getlist("users_accept")
            
            for unIdUser in lesUsers :
                
                leMembre = Membre.objects.get(utilisateur=unIdUser)
                # On enleve la demande
                membre.demandes.remove(leMembre)
                
                if "quoi" in request.POST and "Accepter" in request.POST.get("quoi") :
                    # On ajoute en amis 
                    membre.amis.add(leMembre)
                    
            membre.save()        
    
    autresMembres = Membre.objects.exclude(id=membre.id)

    if membre.amis.count() == 0 :
        amisListe = []
    else :
        amisListe = membre.amis.all()
        autresMembres = autresMembres.exclude(id=amisListe)
        
    if membre.demandes.count() == 0 :
        demandesListe = []
    else :
        demandesListe = membre.demandes.all()
        dmdIds = []
        for dmd in demandesListe:
            dmdIds.append(dmd.id)
        autresMembres = autresMembres.exclude(id__in=dmdIds)
    
    add_friend_form = AddFriendsForm(users=User.objects.filter(membre__in=autresMembres))
    accept_friend_form = AcceptFriendsForm(users=User.objects.filter(membre__in=demandesListe))

    context = {   
        'add_friend_form':add_friend_form,
        'accept_friend_form':accept_friend_form,
        'messages':messages,
        'amis':amisListe,
        'nbDemandesMembres': len(demandesListe),
    }

    return render(request, "dashboard/index.html", context)
