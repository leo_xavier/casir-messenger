from django.conf.urls import patterns, url
from django.views.generic import TemplateView


urlpatterns = patterns('dashboard.views',
                       url('^$', 'index', name="index"),
                       url('^chargerMessage$', 'chargerMessage', name="chargerMessage"),
                       url('^conversation', 'conversation', name="conversation"))
    
