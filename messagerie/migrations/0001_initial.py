# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('duree', models.PositiveSmallIntegerField(choices=[(1, b'1 seconde'), (3, b'3 secondes'), (10, b'10 secondes')])),
                ('date', models.DateTimeField()),
                ('message', models.TextField()),
                ('etat', models.BooleanField(default=False)),
                ('destinataire', models.ForeignKey(related_name='destinataire_set', to='members.Membre')),
                ('expediteur', models.ForeignKey(related_name='expediteur_set', to='members.Membre')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
