from django.db import models
from members.models import Membre

# Create your models here.
class Message(models.Model):
    expediteur = models.ForeignKey(Membre, blank=False, null=False, related_name="expediteur_set")
    destinataire = models.ForeignKey(Membre, blank=False, null=False, related_name="destinataire_set")
    #pj = models.ImageField()
    duree = models.PositiveSmallIntegerField(choices=((1, "1 seconde"),(3, "3 secondes"),(10, "10 secondes")))
    date = models.DateTimeField()
    message = models.TextField()
    etat = models.BooleanField(default=False)