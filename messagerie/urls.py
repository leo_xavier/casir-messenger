from django.conf.urls import patterns, url
from django.views.generic import TemplateView


urlpatterns = patterns('messagerie.views',
                       url('^$', 'index', name="index"))
