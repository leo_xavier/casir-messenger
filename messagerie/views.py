from django.shortcuts import render
from django.contrib.auth import login as auth_login
from django.contrib.auth import authenticate
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from itertools import chain
from datetime import datetime

from messagerie.forms import SendMessageForm
from members.models import Membre
from messagerie.models import Message

def index(request):
    
    user = request.user
    membre = Membre.objects.get(utilisateur=user)
    
    send_msg_form = SendMessageForm(amis=membre.amis.all())
    
    if request.method == "POST":
        
        if "envoyer" in request.POST :
            
            # Destinataire
            idMembreDest = request.POST.get("destinataire")
            membreDest = Membre.objects.get(id=idMembreDest)
            
            # Duree
            nbSecondesDuree = request.POST.get("duree")
            
            # Message
            contenu = request.POST.get("contenu")
            
            msg = Message()
            msg.expediteur = membre
            msg.destinataire = membreDest
            msg.duree = int(nbSecondesDuree)
            msg.date = datetime.now()
            msg.message = contenu
            msg.save()
            
            return HttpResponseRedirect('/dashboard/index/')
    
    context = {
        'send_msg_form': send_msg_form
    }
    
    return render(request, "messagerie/index.html", context)
