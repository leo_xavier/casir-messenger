from django import forms
from django.contrib.auth.models import User
from members.models import Membre
from django.contrib.auth.forms import AuthenticationForm as DjangoAuthenticationForm

class SendMessageForm(forms.Form):   
    duree = forms.ChoiceField(choices=[])
    destinataire = forms.ChoiceField( choices=Membre.objects.none())
    contenu = forms.CharField(label="Message", max_length=254, min_length=2, widget=forms.Textarea(attrs={"class": "form-control", 'placeholder':'Tapez votre texte ici'}))

    def __init__(self, amis):
        super(SendMessageForm,self).__init__()
        choix_duree = (('3', '3 secondes',), ('5', '5 secondes',), ('10', '10 secondes',))
        self.fields["duree"].choices = choix_duree   
        self.fields["destinataire"].choices = [(ami.id, ami.utilisateur.username) for ami in amis]       
    