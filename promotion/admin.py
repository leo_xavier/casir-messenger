from django.contrib import admin
from promotion.models import Question, Choice

admin.site.register(Question)
admin.site.register(Choice)