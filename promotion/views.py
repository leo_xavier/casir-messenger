from django.shortcuts import render
from django.contrib.auth import login as auth_login
from django.contrib.auth import authenticate
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

from members.forms import AuthenticationForm
from members.forms import RegistrationForm
from members.models import Membre

def index(request):

    messaging_url = "/dashboard/index/" # Are we going to a static template ?
    
    authentication_form = AuthenticationForm(request, prefix='authentication')
    registration_form = RegistrationForm(prefix='registration')
    print(request)
    if request.method == "POST":
        user = None
        if "login" in request.POST:
            authentication_form = AuthenticationForm(request, data=request.POST, prefix='authentication')
            if authentication_form.is_valid():
                # Okay, security check complete. Get the user.
                user = authentication_form.get_user()
                
        if user is not None:
            auth_login(request, authentication_form.get_user())
            return HttpResponseRedirect(messaging_url)
            #return render(request, "/dashboard/index/")
        
        elif "register" in request.POST:
            
            registration_form = RegistrationForm(data=request.POST, prefix='registration')
            if registration_form.is_valid():
                data = registration_form.cleaned_data
                user = User.objects.create_user(username=data["username"], 
                                                email=data["email"],
                                                password=data["password"])
                user.save()
                user = authenticate(username=data["username"], password=data["password"])
                print(user)
                membre = Membre.create(user)
                membre.save()
                print(membre)
                
        
                if user is not None:
                        auth_login(request, user)
                        return render(request, "dashboard/index.html")
    
    context = {
        'authentication_form': authentication_form,
        'registration_form': registration_form,
    }

    return render(request, "promotion/index.html", context)
